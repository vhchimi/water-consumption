/*
Especificações de projeto:
ADC 16 bits
Leitura de 2000L/dia
hora
dia
mes
ano

Calcular em ml
*/

#include <stdio.h>
#include <stdlib.h>

#define SAMPLES 5 // numero de amostras para acumular a quantidade de tempo e agua

typedef struct{
    float hora;
    float dia;
    float mes;
    float ano;   
    int qtd;
}storage;

storage *stg;

void start_storage(){
    stg = malloc(sizeof(storage));
    stg->qtd = 0;
    stg->ano = 0;
    stg->mes = 0;
    stg->dia = 0;
    stg->hora = 0;
}

void water_consumption_reading (int last_read_time_interval, int last_measurement){
    int min = last_read_time_interval;
    
    if(min <= 0){
        printf("Erro!");
    }
    else{
        stg->hora += min/60;
        stg->dia = stg->hora/24;
        stg->mes = stg->dia/30;
        stg->ano = stg->mes/12;
        stg->qtd += last_measurement;
    }

}

int main(){
    start_storage();
    int i=0;
    while(i < SAMPLES){
        water_consumption_reading(525600,2000);
        i++;
    }
    printf("%d ml de agua\nEm horas: %.4f\nEm dias: %.4f\nEm meses: %.4f\nEm anos: %.4f\n",stg->qtd,stg->hora,stg->dia,stg->mes,stg->ano);
    free(stg);
    return 0;
}