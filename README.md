# Bem vindo ao teste de habilidades InoBram.

Este teste tem como objetivo avaliar suas habilidades com versionamento, lógica de programação e a sua familiaridade com a linguagem de programação;

# O Projeto

A descrição do problema está no contexto da pergunta disponibilizada em seu desafio. A linguagem escolhida para desenvolvimento é C.
Passo a passo e tecnologias utilizadas encontram-se em <a href='./docs/README.md'>documentação</a>.
## Desafio

Seu desafio possui o cabeçalho de uma função:

```
water_consumption_reading (int last_read_time_interval, int last_measurement)
```

> **last_read_time_interval**: intervalo de tempo entre a leitura atual e anterior, valor em **min**;  

> **last_measurement**: ultimo valor lido da sonda em **ml**;

As entradas dever ser simuladas pela aplicação e as saídas devem ser o cosumo de agua em **ml** por **hora**, **dia**, **mês** e **ano**;