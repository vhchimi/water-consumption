# Documentação
Este arquivo tem a função de documentar, especificando e detalhando o projeto de leitura de uma sonda de água.

<a id='Requisitos'></a>

## Requisitos 
Foram utilizados para atingir o objetivo as seguintes tecnologias:

* gcc version 6.3.0 (MinGW.org GCC-6.3.0-1)
* GNU Make 3.82.90
* git version 2.33.1

E para alcançar o objetivo deste trabalho, foi desenvolvida a seguinte função:
## water_consumption_reading
Que é responsável por fazer a conversão do total de tempo recebidos e acumulados em minutos, para horas, dias, meses e anos. Desta maneira não perdendo a referência dos dados anteriores, e armazenando em uma struct com manejo através de ponteiros.

Parâmetros:

* last_read_time_interval: intervalo de tempo entre a leitura atual e anterior, valor em min do tipo inteiro;
* last_measurement: ultimo valor lido da sonda em ml do tipo inteiro;

Retorno: 

* NULL;

## Execução
Para executar o projeto são necessários os [requisitos](#Requisitos) da seção Documentação.
Após isso, siga os passos:

1. Abra um terminal dentro da pasta **./src**
2. Execute o comando **make** ou **mingw32-make.exe**
3. Execute o comando **make run** ou **mingw32-make.exe run**